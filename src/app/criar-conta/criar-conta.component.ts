import { Component, OnInit } from '@angular/core';
import { BackendService } from '../service/backend.service';

@Component({
  selector: 'app-criar-conta',
  templateUrl: './criar-conta.component.html',
  styleUrls: ['./criar-conta.component.css']
})
export class CriarContaComponent implements OnInit {
  usuario : string = "";
  email : string = "";
  senha : string = "";

  constructor(private service : BackendService) { }

  ngOnInit(): void {
  }
  public criarConta(){
    console.log('clicou no botao!!');
    console.log("usuario: " + this.usuario);
    console.log("email :" + this.email);
    console.log("senha: " + this.senha);
    
    this.service.cadastrarADM(this.usuario,this.email,this.senha).subscribe(resultado =>{
      console.log(resultado);
      this.limparTela()
    }, error =>{
      console.error(error);    
    })  
  }
  private limparTela(){
    this.usuario = ""
    this.email = ""
    this.senha = "";
  }
}
