import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CriarContaComponent } from "./criar-conta/criar-conta.component";
import { LoginComponent } from "./login/login.component";


export const routes : Routes = [
    {path: "", redirectTo: "login", pathMatch: "full"},
    {path: "login", component: LoginComponent },
    {path: "criarConta", component:CriarContaComponent }

]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule{

}
