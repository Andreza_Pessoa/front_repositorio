
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  private url = "http://localhost:3003"
  constructor(private httpClient: HttpClient) { }

  public logarAdm(email : string, senha: string) : Observable<Object>{

  const usuario = {
       email, senha
  }
  
   return  this.httpClient.post<Object>(`${this.url}/login-adm`,usuario)
  }

  public cadastrarADM( usuario: string,email : string, senha: string) : Observable<Object>{

    const novoUsuario = {
      usuario,email, senha
    }
    
     return  this.httpClient.post<Object>(`${this.url}/cadastro-adm`,novoUsuario)
    }
}
