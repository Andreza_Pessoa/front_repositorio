import { Component, OnInit } from '@angular/core';
import { BackendService } from "../service/backend.service";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  senha : string = "";
  email : string = "";
  
  constructor(private service : BackendService) { }

  ngOnInit(): void {
  }
  public login(){

    console.log('clicou');
    console.log("Senha"+ this.senha);
    console.log("email" + this.email);
   
    this.service.logarAdm(this.email, this.senha).subscribe(resultado =>{
      console.log(resultado);
      this.limparTela()   
    },error =>{
      console.error(error);   
    })
    
  }
  private limparTela(){
    this.email = ""
    this.senha = "";
  }
}
